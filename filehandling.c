#include <stdio.h>
int main()
{
	int con;
	FILE *file_1, *file_2, *file_3;

	file_1 = fopen("assignment9.txt", "w" );
	fprintf(file_1, "UCSC is one of the leading institutes in Sri Lanka for computing studies. ");
	fclose(file_1);

	file_2 = fopen ("assignment9.txt", "r" );
	while ((con = getc(file_2)) != EOF)
	{
		putchar(con);
	}
	fclose(file_2);

	file_3 = fopen ("assignment9.txt", "a");
	fprintf(file_3, "UCSC offers undergraduate and postgraduate level courses aiming a range of computing fields. ");
	fclose(file_3);

	return 0;
}
